Aplikaci lze spustil z příkazové řádky ze složky projektu v Git repozitáři v master větvi příkazem grails app-run

Pro implementaci jsem využil Grails a jejich základní vygenerovaný projekt. 
Pro perzistenci je použita H2. Načítání projektů s pomocí Ajaxu a stránkování je s použitím knihovny Datatables  

## Stránka s nastavení jména a hesla
http://localhost:8086/configuration/

## Json Endpoint 
http://localhost:8086/project/api

## Stránka s přehledem projektů načtených z Memsource REST Api
http://localhost:8086/project/

## Spuštění testů
grails test-app