package cz.krisica.memsource

import grails.testing.gorm.DomainUnitTest
import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class ConfigurationControllerSpec extends Specification implements ControllerUnitTest<ConfigurationController>,
        DomainUnitTest<Configuration> {

    void "Test the show configuration from domain service"() {
        given:
        def configuration = new Configuration(username: 'johndoe@gmail.com', password: 'password123')
                .save(flush: true, failOnError: true)

        controller.configurationService = Mock(ConfigurationService) {
            1 * singleConfig() >> configuration
        }

        when: "A domain instance is passed to the index action"
        controller.index()

        then: "A model is populated containing the domain instance"
        model.configuration instanceof Configuration

        and: "And configuration is johndoe@gmail.com with password password123"
        model.configuration.username == 'johndoe@gmail.com'
        model.configuration.password == 'password123'
    }

    void "Test the successful create action redirected to /"() {
        given:
        def configuration = new Configuration(username: 'johndoe@gmail.com', password: 'password123')
                .save(flush: true, failOnError: true)

        controller.configurationService = Mock(ConfigurationService) {}

        controller.memsourceApiService = Mock(MemsourceApiService) {}

        when: "The create action is executed"
        request.method = 'POST'
        controller.save(configuration)

        then:
        response.status == 200
    }

}






