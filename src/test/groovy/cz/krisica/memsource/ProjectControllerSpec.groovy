package cz.krisica.memsource

import com.memsource.api.dto.Page
import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class ProjectControllerSpec extends Specification implements ControllerUnitTest<ProjectController> {

    void 'If Memsource REST api with parameters is called, project list response and a 200 is returned'() {
        given:
        Integer draw = 1
        Integer recordsTotal = 1
        Integer recordsFiltered = 1
        List<List[]> data = []
        List<String> project = []

        project << "test 2"
        project << "NEW"
        project << "de"
        project << "\"cs_cz\""

        data << project

        controller.memsourceApiService = Stub(MemsourceApiService) {
            listAllProjects(_, _) >> new Page(draw: draw, recordsTotal: recordsTotal, recordsFiltered: recordsFiltered, data: data)
        }

        when: 'Request with params is sent'
        request.method = 'POST'
        request.addParameter("draw", "1")
        request.addParameter("length", "10")

        controller.api()

        then: 'Json response with single project is returned'
        response.json.draw == 1
        response.json.recordsTotal == 1
        response.json.recordsFiltered == 1
        response.json.data[0][0] == "test 2"
        response.json.data[0][1] == "NEW"
        response.json.data[0][2] == "de"
        response.json.data[0][3] == "\"cs_cz\""

        response.status == 200
    }

}






