package com.memsource.api

import com.memsource.api.dto.LoginResponse
import com.memsource.api.dto.Page
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.grails.web.json.JSONElement

@CompileStatic
class MemsourceApiParser {

    @CompileDynamic
    static LoginResponse loginResponseFromJSONElement(JSONElement json) {
        LoginResponse loginResponse = new LoginResponse()

        if (json.token) {
            loginResponse.token = json.token
        }

        //TODO json.expires "yyyy-MM-dd'T'HH:mm:ssZ"

        loginResponse
    }

    @CompileDynamic
    static Page projectPageFromJSONElement(JSONElement json) {
        Page page = new Page()

        if (json.pageNumber != null) {
            page.draw = (json.pageNumber as Integer) + 1
        }

        if (json.totalElements) {
            page.recordsTotal = json.totalElements as Integer
        }

        if (json.numberOfElements) {
            page.recordsFiltered = json.numberOfElements as Integer
        }

        if (json.content) {
            page.data = []
            for (Object obj : json.content) {
                List<String> project = projectFromJsonElement(obj)
                page.data << project
            }
        }
        page
    }

    @CompileDynamic
    static List<String> projectFromJsonElement(JSONElement json) {

        List<String> project = []

        if (json.name) {
            project << json.name
        }
        if (json.status) {
            project << json.status
        }

        if (json.sourceLang) {
            project << json.sourceLang
        }

        if (json.targetLangs) {
            project << json.targetLangs.join(',').toString()
        }
        project
    }
}
