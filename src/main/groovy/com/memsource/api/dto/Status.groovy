package com.memsource.api.dto

import groovy.transform.CompileStatic

@CompileStatic
enum Status {
    NEW, ASSIGNED, COMPLETED, ACCEPTED_BY_VENDOR, DECLINED_BY_VENDOR, COMPLETED_BY_VENDOR, CANCELLED

    static Status statusWithString(String str) {

        for (Status status : values()) {
            if (str.toUpperCase().equals(status.name())) {
                return status;
            }
        }
        throw new IllegalArgumentException("status name is not valid " + str)
    }
}