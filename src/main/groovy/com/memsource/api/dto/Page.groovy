package com.memsource.api.dto;

import groovy.transform.CompileStatic;

@CompileStatic
class Page {

    Integer draw
    Integer recordsTotal
    Integer recordsFiltered
    List<List[]> data

}
