package cz.krisica.memsource

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification

@Integration
@Rollback
class ConfigurationIntegrationSpec extends Specification {

    ConfigurationService configurationService

    void "Test save single configuration"() {
        when:
        Configuration configuration = new Configuration(username: 'johndoe@gmail.com', password: 'password123')
        configurationService.save(configuration)

        then:
        configuration.id != null
    }

    void "If try to store more than one configuration throw IllegalStateException"() {
        when:
        Configuration configuration = new Configuration(username: 'johndoe@gmail.com', password: 'password123')
        Configuration configuration2 = new Configuration(username: 'johndoe@gmail.com', password: 'password123')
        configurationService.save(configuration)
        configurationService.save(configuration2)

        then:
        thrown IllegalStateException
    }

}