<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/project')}"><g:message code="project.list.label"/></a></li>
    </ul>
</div>

<div id="edit-configuration" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="['konfiguraci']"/></h1>

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:hasErrors bean="${this.configuration}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.configuration}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <g:form resource="${this.configuration}" method="POST">
        <fieldset class="form">
            <f:all bean="configuration"/>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="save"
                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
        </fieldset>
    </g:form>

</div>
</body>
</html>
