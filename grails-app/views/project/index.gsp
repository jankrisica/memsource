<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="jquery.dataTables.min.css"/>
    <asset:javascript src="jquery-3.3.1.min.js"/>
    <asset:javascript src="jquery.dataTables.min.js"/>
</head>

<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/configuration')}"><g:message code="configuration.label"/></a></li>
    </ul>
</div>

<div class="content scaffold-list" role="main">

    <h1><g:message code="projects.list.header"/></h1>

    <table id="project_table_id" class="display" style="width:100%">
        <thead>
        <tr>
            <th><g:message code="project.name"/></th>
            <th><g:message code="project.status"/></th>
            <th><g:message code="project.sourceLang"/></th>
            <th><g:message code="project.targetLang"/></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th><g:message code="project.name"/></th>
            <th><g:message code="project.status"/></th>
            <th><g:message code="project.sourceLang"/></th>
            <th><g:message code="project.targetLang"/></th>
        </tr>
        </tfoot>
    </table>

</div>
<g:javascript>
        $(document).ready( function () {
            $.noConflict();
            $('#project_table_id').DataTable({
               "processing": true,
               "serverSide": true,
               "searching": false,
               "bLengthChange": false,
               "ajax": '${createLink(controller: 'project', action: 'api')}'
            });
        } );
</g:javascript>
</body>
</html>