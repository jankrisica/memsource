package cz.krisica.memsource

import grails.config.Config
import grails.converters.JSON
import grails.core.support.GrailsConfigurationAware

class ProjectController implements GrailsConfigurationAware {

    def memsourceApiService

    Integer defaultPageSize

    @Override
    void setConfiguration(Config co) {
        defaultPageSize = co.getProperty('memsource.api.defaultPageSize', Integer)
    }

    def index() {
        respond([])
    }

    def api() {
        response.setContentType("application/json")

        def pageNumber = params.int('draw');
        def pageSize = params.int('length', defaultPageSize);

        def projects = memsourceApiService.listAllProjects(pageNumber, pageSize)

        if (projects == null) {
            render status: 503, text: "Resource not available"
            return
        }

        render(projects as JSON)
    }
}
