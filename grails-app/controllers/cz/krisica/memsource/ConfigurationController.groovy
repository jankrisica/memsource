package cz.krisica.memsource

import grails.gorm.transactions.Transactional
import grails.validation.ValidationException

import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

class ConfigurationController {

    def configurationService

    def memsourceApiService

    static allowedMethods = [save: "POST"]


    def index() {
        def config = configurationService.singleConfig()

        if (config == null) {
            respond new Configuration(params)
        } else {
            respond config
        }
    }

    @Transactional
    def save(Configuration configuration) {
        if (configuration == null) {
            notFound()
            return
        }

        try {
            configurationService.save(configuration)
        } catch (ValidationException e) {
            respond configuration.errors, view: 'edit'
            return
        }

        memsourceApiService.initApi()

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'configuration.label', default: 'Configuration'), configuration.id])
                redirect action: "", method: "GET"
            }
            '*' { respond configuration, [status: OK] }
        }
    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'configuration.label', default: 'Configuration'), params.id])
                redirect action: "", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
