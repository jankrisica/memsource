package cz.krisica.memsource

import com.memsource.api.MemsourceApiParser
import com.memsource.api.dto.LoginResponse
import com.memsource.api.dto.Page
import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import groovy.json.JsonBuilder
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

@CompileStatic
class MemsourceApiService implements GrailsConfigurationAware {

    def configurationService

    String memsourceApiUrl

    String token;

    @Override
    void setConfiguration(Config co) {
        memsourceApiUrl = co.getProperty('memsource.api.url', String)
    }

    @CompileDynamic
    def initApi() {

        def config = configurationService.singleConfig()

        if (config) {
            println "Configuration is available"
            def loginResponse = retrieveToken(config)
            if (loginResponse != null) {
                token = loginResponse.token
                println "Api initialized"
            } else {
                token = null
                println "Api initialization failed"
            }
        }

    }

    @CompileDynamic
    LoginResponse retrieveToken(Configuration configuration) {
        RestBuilder rest = new RestBuilder()
        String url = "${memsourceApiUrl}/auth/login"

        JsonBuilder jsonPayload = new JsonBuilder([
                "userName": configuration.username,
                "password": configuration.password
        ])

        def restResponse = rest.post(url) {
            accept("application/json")
            contentType("application/json")
            body(jsonPayload.toPrettyString())
        }

        if (restResponse.statusCode.value() == 200 && restResponse.json) {
            MemsourceApiParser.loginResponseFromJSONElement(restResponse.json)
        }
    }

    @CompileDynamic
    Page listAllProjects(pageNumber, pageSize) {
        if (token == null) {
            initApi()
        }
        RestBuilder rest = new RestBuilder()

        String url = "${memsourceApiUrl}/projects?token={token}&pageSize={pageSize}"
        Map params = [token: token, pageSize: pageSize]

        if (pageNumber != null && pageNumber > 1) {
            params.pageNumber = pageNumber
            url += "&pageNumber={pageNumber}"
        }

        RestResponse restResponse = rest.get(url) {
            urlVariables params
        }

        if (restResponse.statusCode.value() == 200 && restResponse.json) {
            return MemsourceApiParser.projectPageFromJSONElement(restResponse.json)
        }
        null
    }


}