package cz.krisica.memsource

import grails.gorm.transactions.Transactional

@Transactional
class ConfigurationService {

    def save(configuration) {
        if (Configuration.list().isEmpty() || configuration.id != null) {
            configuration.save()
        } else {
            throw new IllegalStateException("Configuration already exists")
        }
    }

    def singleConfig() {
        def list = Configuration.list()
        list.isEmpty() ? null : list.iterator().next()
    }
}
